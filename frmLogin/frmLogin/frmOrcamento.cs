﻿using frmLogin.Cadastrar_Carro;
using frmLogin.Orçamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmOrcamento : Form
    {
        BindingList<CarroDTO> produtosCarrinho = new BindingList<CarroDTO>();

        public frmOrcamento()
        {
            InitializeComponent();
            CarregarCombo();
            
        }

        void CarregarCombo()
        {
            CarroBusiness business = new CarroBusiness();
            List<CarroDTO> lista = business.Listar();

            cboCarro.ValueMember = nameof(CarroDTO.Id);
            cboCarro.DisplayMember = nameof(CarroDTO.Carro);
            cboCarro.DataSource = lista;
        }


        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            CarroDTO dto = cboCarro.SelectedItem as CarroDTO;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();

        }

        private void button3_Click(object sender, EventArgs e)
        {
            CarroDTO car = cboCarro.SelectedItem as CarroDTO;

            OrcamentoDTO dto = new OrcamentoDTO();
            dto.id_carro = car.Id;

            dto.nm_valor1 = txtValor1.Text;
            dto.nm_valor2 = txtValor2.Text;
            dto.nm_valor3 = txtValor3.Text;
            dto.nm_defeito = txtDefeito.Text;

            OrcamentoBusiness business = new OrcamentoBusiness();
            business.Salvar(dto);

            MessageBox.Show("Orçamento salvo com sucesso.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void frmOrcamento_Load(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }

        private void txtValor1_KeyPress(object sender, KeyPressEventArgs e)
        {
        
        }
    }
}
