﻿using frmLogin.Orçamento;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmConsultarOrcamento : Form
    {
        public frmConsultarOrcamento()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void frmConsultarOrcamento_Load(object sender, EventArgs e)
        {
            OrcamentoBusiness business = new OrcamentoBusiness();
            List<OrcamentoDTO> lista = business.Listar();

            dgvPedido.AutoGenerateColumns = false;
            dgvPedido.DataSource = lista;
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }
}
