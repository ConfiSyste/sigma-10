﻿using frmLogin.PedidodeCompra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin.Estoque
{
    public partial class frmEstoque : Form
    {
        public frmEstoque()
        {
            InitializeComponent();
        }

        private void dgvCarros_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void frmEstoque_Load(object sender, EventArgs e)
        {
            PedidoCompraBusiness bus = new PedidoCompraBusiness();
            List<PedidoCompraDTO> lista = bus.Listar();

            dgvEstoque.AutoGenerateColumns = false;
            dgvEstoque.DataSource = lista;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }
    }
}
