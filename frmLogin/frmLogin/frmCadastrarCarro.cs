﻿using frmLogin.Cadastrar_Carro;
using System;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmCadastrarCarro : Form
    {
        public frmCadastrarCarro()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            CarroDTO dto = new CarroDTO();
            dto.Carro = txtModelo.Text;
            dto.Placa = txtPlaca.Text;
            dto.Dono = txtDono.Text;

            if (dto.Carro == string.Empty || dto.Carro == "Digitar o Carro")
            {
                MessageBox.Show("Digite o Carro");

                frmCadastrarCarro ir = new frmCadastrarCarro();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Placa == string.Empty || dto.Placa == "Digitar o Placa")
            {
                MessageBox.Show("Digite a Placa");

                frmCadastrarCarro ir = new frmCadastrarCarro();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Dono == string.Empty || dto.Dono == "Digitar o Proprietário")
            {
                MessageBox.Show("Digite a Proprietário");

                frmCadastrarCarro ir = new frmCadastrarCarro();
                this.Hide();
                ir.ShowDialog();
            }

            CarroBusiness business = new CarroBusiness();
            business.Salvar(dto);

            MessageBox.Show("Carro salvo com sucesso.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void frmCadastrarCarro_Load(object sender, EventArgs e)
        {

        }

        private void txtModelo_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) == true || e.KeyChar == (char)Keys.Space || e.KeyChar == (char)Keys.Back || char.IsLetter(e.KeyChar) == true)
            {
                e.Handled = false;

                if (txtModelo.Text.Length > 15 && e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void txtPlaca_KeyPress(object sender, KeyPressEventArgs e)
        {
            
        }
    }
}
