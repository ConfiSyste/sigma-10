﻿using frmLogin.Fornecedor;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmFornecedor : Form
    {
        public frmFornecedor()
        {
            InitializeComponent();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            FornecedorDTO dto = new FornecedorDTO();
            dto.Nome = txtNome.Text;
            dto.Cnpj = txtCNPJ.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Rua = txtRua.Text;
            dto.Bairro = txtBairro.Text;
            dto.CadastroFornecedor = txtCadastroFornecedor.Text;
            dto.Email = txtEmail.Text;
            dto.DenominacaoSocial = txtDenominacaoSocial.Text;
            dto.RamoAtividade = txtRamoAtividade.Text;
            dto.Funcao = txtFuncao.Text;
            dto.Entrega = txtEntrega.Text;
            dto.Desconto = txtDesconto.Text;



            if (dto.Nome == string.Empty || dto.Nome == "Digitar nome")
            {
                MessageBox.Show("Digite o Nome");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.CadastroFornecedor == string.Empty || dto.CadastroFornecedor == "Digite o cadastro de fornecedor")
            {
                MessageBox.Show("Digite o cadastro");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.DenominacaoSocial == string.Empty || dto.DenominacaoSocial == "Digite a denominacao")
            {
                MessageBox.Show("Digite a denominacao social");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.Rua == string.Empty || dto.Rua == "Digite a rua")
            {
                MessageBox.Show("Digite a rua");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }


            if (dto.Bairro == string.Empty || dto.Bairro == "Digite o Bairro")
            {
                MessageBox.Show("Digite a rua");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Email == string.Empty || dto.Email == "Digite o Email")
            {
                MessageBox.Show("Digite o Email");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Cnpj == string.Empty || dto.Cnpj == "Digite o Cnpj")
            {
                MessageBox.Show("Digite o CNPJ");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.Inscricao == string.Empty || dto.Inscricao == "Digite sua Inscricao")
            {
                MessageBox.Show("Digite sua Inscricao");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.Telefone == string.Empty || dto.Telefone == "Digitar telefone")
            {
                MessageBox.Show("Digite o Telefone");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }



            if (dto.RamoAtividade == string.Empty || dto.RamoAtividade == "Digite o ramo de atividade")
            {
                MessageBox.Show("Digite o ramo de atividade");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.Funcao == string.Empty || dto.Funcao == "Digite a funcao")
            {
                MessageBox.Show("Digite a funcao");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.Entrega == string.Empty || dto.Entrega == "Digite a data de entrega")
            {
                MessageBox.Show("Digite a data de entrega");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }
            if (dto.Desconto == string.Empty || dto.Desconto == "Digite o desconto")
            {
                MessageBox.Show("Digite o desconto");

                frmFornecedor ir = new frmFornecedor();
                this.Hide();
                ir.ShowDialog();
            }





            FornecedorBusiness business = new FornecedorBusiness();
            business.Salvar(dto);

            MessageBox.Show("Fornecedor salvo com sucesso.", "Peças", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void btnConsultarFornecedor_Click(object sender, EventArgs e)
        {
            frmConsultarFornecedor ir = new frmConsultarFornecedor();
            this.Hide();
            ir.ShowDialog();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void label8_Click(object sender, EventArgs e)
        {

        }
    }
}
