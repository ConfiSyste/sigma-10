﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoDataBase
    {
        public int Salvar(OrcamentoDTO dto)
        {
            string script = @"INSERT INTO TB_ORCAMENTO (id_carro, nm_valor1, nm_valor2, nm_valor3, nm_defeito) VALUES (@id_carro, @nm_valor1, @nm_valor2, @nm_valor3, @nm_defeito)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", dto.id_carro));
            parms.Add(new MySqlParameter("nm_valor1", dto.nm_valor1));
            parms.Add(new MySqlParameter("nm_valor2", dto.nm_valor2));
            parms.Add(new MySqlParameter("nm_valor3", dto.nm_valor3));
            parms.Add(new MySqlParameter("nm_defeito", dto.nm_defeito));


            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(OrcamentoDTO dto)
        {
            string script = @"UPDATE TB_ORCAMENTO 
                                 SET id_carro = @id_carro,
                                     nm_valor1 = @nm_valor1,
                                     nm_valor2 = @nm_valor2,
                                     nm_valor3 = @nm_valor3,
                                     nm_defeito = @nm_defeito
                               WHERE id_produto = @id_produto";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", dto.id_carro));
            parms.Add(new MySqlParameter("nm_valor1", dto.nm_valor1));
            parms.Add(new MySqlParameter("nm_valor2", dto.nm_valor2));
            parms.Add(new MySqlParameter("nm_defeito", dto.nm_defeito));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM TB_ORCAMENTO WHERE id_pedido = @id_pedido";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScript(script, parms);
        }


        public List<OrcamentoDTO> Listar()
        {
            string script = @"SELECT * FROM TB_ORCAMENTO";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<OrcamentoDTO> lista = new List<OrcamentoDTO>();
            while (reader.Read())
            {
                OrcamentoDTO dto = new OrcamentoDTO();
                dto.id_carro = reader.GetInt32("id_carro");
                dto.nm_valor1 = reader.GetString("nm_valor1");
                dto.nm_valor2 = reader.GetString("nm_valor2");
                dto.nm_valor2 = reader.GetString("nm_valor3");
                dto.nm_defeito = reader.GetString("nm_defeito");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<OrcamentoConsultarView> Consultar(string carro)
        {
            string script = @"SELECT * FROM vw_pedido_consultar WHERE id_carro like @id_carro";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_carro", carro + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<OrcamentoConsultarView> lista = new List<OrcamentoConsultarView>();
            while (reader.Read())
            {
                OrcamentoConsultarView dto = new OrcamentoConsultarView();
                dto.Id = reader.GetInt32("id_carro");
                dto.Carro = reader.GetString("id_cliente");
                dto.Valor1 = reader.GetString("nm_valor1");
                dto.Valor2 = reader.GetString("nm_valor2");
                dto.Valor3 = reader.GetString("nm_valor3");
                dto.Defeito = reader.GetString("nm_defeito");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
