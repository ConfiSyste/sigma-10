﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoItemDTO
    {
        public int Id { get; set; }
        public int IdOrcamento { get; set; }
        public int IdCarro { get; set; }
    }
}
