﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoDTO
    {
        public int id_carro { get; set; }
        public string carro { get; set; }
        public string nm_valor1 { get; set; }
        public string nm_valor2 { get; set; }
        public string nm_valor3 { get; set; }
        public string nm_defeito { get; set; }
    }
}
