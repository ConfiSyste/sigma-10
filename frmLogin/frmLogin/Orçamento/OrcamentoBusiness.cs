﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Orçamento
{
    class OrcamentoBusiness
    {
        public int Salvar(OrcamentoDTO pedido)
        {
            OrcamentoDataBase orcamentoDatabase = new OrcamentoDataBase();
            int idPedido = orcamentoDatabase.Salvar(pedido);

            return idPedido;
        }

        public List<OrcamentoDTO> Listar()
        {
            OrcamentoDataBase db = new OrcamentoDataBase();
            return db.Listar();
        }
    }
}
