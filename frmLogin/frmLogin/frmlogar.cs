﻿using frmLogin.Login;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmlogar : Form
    {
        public frmlogar()
        {
            InitializeComponent();
        }

        private void frmLogar_Load(object sender, EventArgs e)
        {

        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void label15_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            string cnome1 = txtnome2.Text;

            string senha1 = txtsenha2.Text;
            LoginBusiness business = new LoginBusiness();
            bool logou = business.Logar(cnome1, senha1);


            if (logou == true)
            {
                frmMenu telapedido = new frmMenu();
                this.Hide();
                telapedido.ShowDialog();

            }
            else
            {
                MessageBox.Show("Dados inválidos", "Erro", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            frmCadastrar cadastrar = new frmCadastrar();
            this.Hide();
            cadastrar.ShowDialog();
        }

        private void label1_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void txtnome2_KeyPress(object sender, KeyPressEventArgs e)
        {

        }
    }
}
