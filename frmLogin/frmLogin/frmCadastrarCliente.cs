﻿using frmLogin.Cadastrar_Cliente;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmCadastrarCliente : Form
    {
        public frmCadastrarCliente()
        {
            InitializeComponent();
        }

        private void txtNumero_TextChanged(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            ClienteDTO dto = new ClienteDTO();
            dto.Nome = txtNome.Text;
            dto.Cpf = txtCpf.Text;
            dto.Nascimento = txtData.Text;
            dto.Telefone = txtTelefone.Text;
            dto.Rua = txtRua.Text;
            dto.Bairro = txtBairro.Text;
            dto.Numero = txtNumero.Text;
            dto.Cep = txtCep.Text;
            dto.Sexo = txtSexo.Text;


            if (dto.Nome == string.Empty || dto.Nome == "Digitar nome")
            {
                MessageBox.Show("Digite o Nome");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Cpf == string.Empty || dto.Cpf == "Digitar cpf")
            {
                MessageBox.Show("Digite o Cpf");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Nascimento == string.Empty || dto.Nascimento == "Digitar Nascimento")
            {
                MessageBox.Show("Digite a Nascimento");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Telefone == string.Empty || dto.Telefone == "Digitar telefone")
            {
                MessageBox.Show("Digite o Telefone");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Rua == string.Empty || dto.Rua == "Digitar rua")
            {
                MessageBox.Show("Digite a  Rua");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Bairro == string.Empty || dto.Bairro == "Digitar bairro")
            {
                MessageBox.Show("Digite o Bairro");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Numero == string.Empty || dto.Numero == "Digitar numero")
            {
                MessageBox.Show("Digite o Numero");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Cep == string.Empty || dto.Cep == "Digitar cep")
            {
                MessageBox.Show("Digite o Cep");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            if (dto.Sexo == string.Empty || dto.Sexo == "Digitar Sexo")
            {
                MessageBox.Show("Digite o Sexo");

                frmCadastrarCliente ir = new frmCadastrarCliente();
                this.Hide();
                ir.ShowDialog();
            }

            ClienteBusiness business = new ClienteBusiness();
            business.Salvar(dto);

            MessageBox.Show("Cliente salvo com sucesso.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void txtNome_Click(object sender, EventArgs e)
        {
            txtNome.Text = string.Empty;
        }

        private void txtRua_Click(object sender, EventArgs e)
        {
            txtRua.Text = string.Empty;
        }

        private void txtNumero_Click(object sender, EventArgs e)
        {
            txtNumero.Text = string.Empty;
        }

        private void txtBairro_Click(object sender, EventArgs e)
        {
            txtBairro.Text = string.Empty;
        }

        private void frmCadastrarCliente_Load(object sender, EventArgs e)
        {
        }

        private void txtNome_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                e.Handled = false;

                if (txtNome.Text.Length > 49 && e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtRua_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                e.Handled = false;

                if (txtRua.Text.Length > 149 && e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtNumero_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space || char.IsNumber(e.KeyChar) == true || e.KeyChar == '-')
            {
                e.Handled = false;

                if (txtNumero.Text.Length > 15 && e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtBairro_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsLetter(e.KeyChar) == true || e.KeyChar == (char)Keys.Back || e.KeyChar == (char)Keys.Space)
            {
                e.Handled = false;

                if (txtBairro.Text.Length > 99 && e.KeyChar != (char)Keys.Back)
                {
                    e.Handled = true;
                }
                else
                {
                    e.Handled = false;
                }
            }
            else
            {
                e.Handled = true;
            }
        }

        private void txtCpf_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (char.IsNumber(e.KeyChar) ||e.KeyChar == (char)Keys.Back)
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
