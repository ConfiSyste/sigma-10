﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.PedidodeCompra
{
    public class PedidoCompraDTO
    {
        public int Id_Pedido_Compra { get; set; }
        public string Produto { get; set; }
        public decimal QT_produto { get; set; }
        public decimal Valor { get; set; }
    }
}
