﻿using frmLogin.PedidodeCompra;
using System;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmPedidodeCompra : Form
    {
        public frmPedidodeCompra()
        {
            InitializeComponent();
        }

        private void frmPedidodeCompra_Load(object sender, EventArgs e)
        {
            
        }

        private void label5_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void lblclose1_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            PedidoCompraDTO dto = new PedidoCompraDTO();

            if (txtVL.Text == string.Empty ||txtProduto.Text == string.Empty|| txtVL.Text == string.Empty)
            {
                MessageBox.Show("Campos Incompletos", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else

            


                dto.Produto = txtProduto.Text;
                dto.QT_produto = Convert.ToDecimal(txtQT.Text);
                dto.Valor = Convert.ToDecimal(txtVL.Text);
                MessageBox.Show("Pedido salvo com sucesso.", "Sigma", MessageBoxButtons.OK, MessageBoxIcon.Information);
                PedidoCompraBusiness business = new PedidoCompraBusiness();
                business.Salvar(dto);
            }

        private void label1_Click(object sender, EventArgs e)
        {

        }
    }

       
    }

