﻿using frmLogin.PedidodeCompra;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class frmPedidodeCompra2 : Form
    {
        public frmPedidodeCompra2()
        {
            InitializeComponent();
            CarregarCombos();
        
        }

        private void frmPedidodeCompra2_Load(object sender, EventArgs e)
        {
            
            

        }
        private void CarregarCombos()
        {
            cbbProduto.DataSource = new PedidoCompraBusiness().Listar();
            cbbProduto.ValueMember = "Id_Pedido_Compra";
            cbbProduto.DisplayMember = "Produto";

        }


        private void button3_Click(object sender, EventArgs e)
        {
            new PedidoCompraBusiness().Salvar(new PedidoCompraDTO
            {
                Id_Pedido_Compra = Convert.ToInt32(cbbProduto.SelectedValue),

            });
            


        }

        private void cbbProduto_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }
    }
}
        

