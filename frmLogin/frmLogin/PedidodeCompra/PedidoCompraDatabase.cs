﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.PedidodeCompra
{
    class PedidoCompraDatabase
    {
        public int Salvar(PedidoCompraDTO dto)
        {
            string script = @"INSERT INTO tb_pedido_compra (Produto, QT_produto, Valor)
                                              VALUES (@Produto, @QT_produto, @Valor)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Produto", dto.Produto));
            parms.Add(new MySqlParameter("QT_produto", dto.QT_produto));
            parms.Add(new MySqlParameter("Valor", dto.Valor));



            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }



        public List<PedidoCompraDTO> Listar()
        {
            string script = @"SELECT * FROM tb_pedido_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoCompraDTO> lista = new List<PedidoCompraDTO>();
            while (reader.Read())
            {
                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.Id_Pedido_Compra = reader.GetInt32("Id_Pedido_Compra");
                dto.Produto = reader.GetString("Produto");
                dto.QT_produto = reader.GetDecimal("QT_produto");
                dto.Valor = reader.GetDecimal("Valor");


                lista.Add(dto);
            }
            reader.Close();

            return lista;
      
        }


       

            public List<PedidoCompraDTO> ListarEstoque()
        {
            string script = @"SELECT * FROM tb_pedido_compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoCompraDTO> lista = new List<PedidoCompraDTO>();
            while (reader.Read())
            {
                PedidoCompraDTO dto2 = new PedidoCompraDTO();
                dto2.Id_Pedido_Compra = reader.GetInt32("Id_Pedido_Compra");
                dto2.Produto = reader.GetString("Produto");
                dto2.QT_produto = reader.GetDecimal("QT_produto");

                lista.Add(dto2);
            }
            reader.Close();

            return lista;

        }
    }
}
