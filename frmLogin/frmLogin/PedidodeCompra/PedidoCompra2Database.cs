﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.PedidodeCompra
{
    class PedidoCompra2Database
    {
        public int Salvar(PedidoCompra2DTO dto)
        {
            string script = @"INSERT INTO tb_pedido_compra (Id_Pedido_Compra)
                                              VALUES (@Id_Pedido_Compra";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Id_Pedido_Compra", dto.Id_Pedido_Compra));




            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }


                    
        public List<PedidoCompraDTO> Listar2(string produto)
        {
            string
            script = @"Id_Pedido_Compra,
                        Valor
                    FROM
                        `Login`.tb_pedido_compra
                    WHERE
                        Id_Pedido_Compra = @Id_Pedido_Compra
                    ORDER BY Valor";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("Id_Pedido_Compra", produto + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<PedidoCompraDTO> lista = new List<PedidoCompraDTO>();
            while (reader.Read())
            {
                PedidoCompraDTO dto = new PedidoCompraDTO();
                dto.Id_Pedido_Compra = reader.GetInt32("Id_Pedido_Compra");
                dto.Valor = reader.GetDecimal("Valor");
                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
