﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace frmLogin
{
    public partial class Calculadora : Form
    {
        string n1 = string.Empty;
        decimal n = 1;
        public Calculadora()
        {
            InitializeComponent();

        }

        private void Calculadora_Load(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            frmMenu ir = new frmMenu();
            this.Hide();
            ir.ShowDialog();
        }

        private void btnSomar_Click(object sender, EventArgs e)
        {
            n = 2;
            n1 = "soma";
        }

        private void btnMultiplicação_Click(object sender, EventArgs e)
        {
            n = 2;
            n1 = "multiplicacao";
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtNumero1.Clear();
            txtNumero2.Clear();
            txtresultado.Clear();
        }

        private void btnSubtracao_Click(object sender, EventArgs e)
        {
            n = 2;
            n1 = "subtracao";
        }

        private void btnDivisão_Click(object sender, EventArgs e)
        {
            n = 2;
            n1 = "divisao";
        }

        private void lblResultado_Click(object sender, EventArgs e)
        {
         
        }

        private void txtNumero1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtNumero2_TextChanged(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {

        }

        private void btn1_Click(object sender, EventArgs e)
        {

            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "1";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "1";
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "2";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "2";
            }
        }

        private void btnEnter_Click(object sender, EventArgs e)
        {
            if (txtNumero1.Text == string.Empty || txtNumero2.Text == string.Empty)
            {
                MessageBox.Show("Campos incompletos!");
                Calculadora ir = new Calculadora();
                this.Hide();
                ir.ShowDialog();

            }


            decimal valor1 = Convert.ToDecimal(txtNumero1.Text);
            decimal valor2 = Convert.ToDecimal(txtNumero2.Text);

            if (n1 == "divisao")
            {
                decimal conta = valor1 / valor2;
                txtresultado.Text = conta.ToString();
            }
            else if (n1 == "multiplicacao")
            {
                decimal conta = valor1 * valor2;
                txtresultado.Text = conta.ToString();
            }
            else if (n1 == "subtracao")
            {
                decimal conta = valor1 - valor2;
                txtresultado.Text = conta.ToString();
            }
            else if (n1 == "soma")
            {
                decimal conta = valor1 + valor2;
                txtresultado.Text = conta.ToString();
            }

            n = 1;
            txtNumero1.Text = string.Empty;
            txtNumero2.Text = string.Empty;
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "3";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "3";
            }
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "4";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "4";
            }
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "5";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "5";
            }
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "6";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "6";
            }
        }

        private void btn7_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "7";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "7";
            }
        }

        private void btn8_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "8";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "8";
            }
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + "9";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + "9";
            }
        }

        private void btnVirgula_Click(object sender, EventArgs e)
        {
            if (n == 1)
            {
                txtNumero1.Text = txtNumero1.Text + ",";
            }
            else if (n == 2)
            {
                txtNumero2.Text = txtNumero2.Text + ",";
            }
        }

    
    }
}
