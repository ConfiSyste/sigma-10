﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Fornecedor
{
    class FornecedorDTO
    {
        public int Id { get; set; }
        public string Nome { get; set; }
        public string Cnpj { get; set; }
        public string CadastroFornecedor { get; set; }
        public string Telefone { get; set; }
        public string Rua { get; set; }
        public string Bairro { get; set; }
        public string Email { get; set; }
        public string Inscricao { get; set; }
        public string DenominacaoSocial { get; set; }
        public string RamoAtividade { get; set; }
        public string Funcao { get; set; }
        public string Entrega { get; set; }
        public string Desconto { get; set; }
    }
}
