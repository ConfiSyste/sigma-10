﻿using frmLogin.Base;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace frmLogin.Fornecedor
{
    class FornecedorDatabase
    {
        public int Salvar(FornecedorDTO dto)
        {
            string script = @"INSERT INTO tb_fornecedor (nm_nome, ds_cnpj, nr_telefone, nm_rua, nm_bairro,ds_cadastrofornecedor,ds_email,ds_denominacaosocial,ds_ramoatividade,ds_funcao,ds_entrega,ds_desconto )
                                              VALUES (@nm_nome, @ds_cnpj, @nr_telefone, @nm_rua, @nm_bairro, @ds_cadastrofornecedor,@ds_email,@ds_denominacaosocial,@ds_ramoatividade,@ds_funcao,@ds_entrega,@ds_desconto)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_cnpj", dto.Cnpj));
            parms.Add(new MySqlParameter("ds_cadastrofornecedor", dto.CadastroFornecedor));
            parms.Add(new MySqlParameter("nr_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nm_rua", dto.Rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_denominacaosocial", dto.DenominacaoSocial));
            parms.Add(new MySqlParameter("ds_ramoatividade", dto.RamoAtividade));
            parms.Add(new MySqlParameter("ds_funcao", dto.Funcao));
            parms.Add(new MySqlParameter("ds_entrega", dto.Entrega));
            parms.Add(new MySqlParameter("ds_desconto", dto.Desconto));

            DataBase db = new DataBase();
            return db.ExecuteInsertScriptWithPk(script, parms);
        }

        public void Alterar(FornecedorDTO dto)
        {
            string script = @"UPDATE tb_fornecedor
                                 SET nm_nome = @nm_nome,
                                     ds_cnpj  = @ds_cnpj,
                                     nr_telefone = @nr_telefone,
                                     nm_rua = @nm_rua,
                                     nm_bairro = @nm_bairro,
                                     ds_cadastrofornecedor = @ds_cadastrofornecedor,
                                     ds_email = @ds_email,
                                     ds_denominacaosocial =  @ds_denominacaosocial
                                     ds_ramoatividade =  @ds_ramoatividade
                                     ds_funcao =  @ds_funcao
                                     ds_entrega =  @ds_entrega
                                     ds_desconto =  @ds_desconto
                               WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", dto.Nome));
            parms.Add(new MySqlParameter("ds_cnpj", dto.Cnpj));
            parms.Add(new MySqlParameter("ds_cadastrofornecedor", dto.CadastroFornecedor));
            parms.Add(new MySqlParameter("nr_telefone", dto.Telefone));
            parms.Add(new MySqlParameter("nm_rua", dto.Rua));
            parms.Add(new MySqlParameter("nm_bairro", dto.Bairro));
            parms.Add(new MySqlParameter("ds_email", dto.Email));
            parms.Add(new MySqlParameter("ds_denominacaosocial", dto.DenominacaoSocial));
            parms.Add(new MySqlParameter("ds_ramoatividade", dto.RamoAtividade));
            parms.Add(new MySqlParameter("ds_funcao", dto.Funcao));
            parms.Add(new MySqlParameter("ds_entrega", dto.Entrega));
            parms.Add(new MySqlParameter("ds_desconto", dto.Desconto));

            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);

        }

        public void Remover(int id)
        {
            string script = @"DELETE FROM tb_fornecedor WHERE id_cliente = @id_cliente";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("id_cliente", id));

            DataBase db = new DataBase();
            db.ExecuteInsertScriptWithPk(script, parms);
        }

        public List<FornecedorDTO> Consultar(string fornecedor)
        {
            string script = @"SELECT * FROM tb_fornecedor WHERE nm_nome like @nm_nome";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_nome", fornecedor + "%"));

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.Cnpj = reader.GetString("ds_cnpj");
                dto.Telefone = reader.GetString("nr_telefone");
                dto.Rua = reader.GetString("nm_rua");
                dto.Bairro = reader.GetString("nm_bairro");
                dto.CadastroFornecedor = reader.GetString("ds_cadastrofornecedor");
                dto.Email = reader.GetString("ds_email");
                dto.DenominacaoSocial = reader.GetString("ds_denominacaosocial");
                dto.RamoAtividade = reader.GetString("ds_ramoatividade");
                dto.Funcao = reader.GetString("ds_funcao");
                dto.Entrega = reader.GetString("ds_entrega");
                dto.Desconto = reader.GetString("ds_desconto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }

        public List<FornecedorDTO> Listar()
        {
            string script = @"SELECT * FROM tb_fornecedor";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            DataBase db = new DataBase();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<FornecedorDTO> lista = new List<FornecedorDTO>();
            while (reader.Read())
            {
                FornecedorDTO dto = new FornecedorDTO();
                dto.Id = reader.GetInt32("id_cliente");
                dto.Nome = reader.GetString("nm_nome");
                dto.Cnpj = reader.GetString("ds_cnpj");
                dto.Telefone = reader.GetString("nr_telefone");
                dto.Rua = reader.GetString("nm_rua");
                dto.Bairro = reader.GetString("nm_bairro");
                dto.CadastroFornecedor = reader.GetString("ds_cadastrofornecedor");
                dto.Email = reader.GetString("ds_email");
                dto.DenominacaoSocial = reader.GetString("ds_denominacaosocial");
                dto.RamoAtividade = reader.GetString("ds_ramoatividade");
                dto.Funcao = reader.GetString("ds_funcao");
                dto.Entrega = reader.GetString("ds_entrega");
                dto.Desconto = reader.GetString("ds_desconto");

                lista.Add(dto);
            }
            reader.Close();

            return lista;
        }
    }
}
